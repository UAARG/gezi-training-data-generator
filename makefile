exe: main.o filters.o images.o
	g++ main.o filters.o images.o -o exe -O2 -L/usr/X11R6/lib -lm -lpthread -lX11 -lpng -ljpeg

main.o: src/main.cpp
	g++ -c src/main.cpp

filters.o: src/filters.cpp src/filters.hpp
	g++ -c src/filters.cpp

images.o : src/images.cpp src/images.hpp
	g++ -c src/images.cpp

clean:
	rm *.o exe && rm Images/Processed\ Images/training/* && rm Images/Processed\ Images/validating/* && rm Images/Processed\ Images/testing/* 
