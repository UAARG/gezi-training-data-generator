///////////////////////////////////////////
// Program: Gezi Training Data Generator
// File: filters.cpp
///////////////////////////////////////////


#include <iostream>
#include <vector>
#include <string>

#define cimg_use_png 1
#define cimg_display 1
#include "../bin/CImg.h"

#include "filters.hpp"




using namespace std;
using namespace cimg_library;

vector<CImg<unsigned char> >Filter::create_filters(int num_of_filters)
{

	for (int i = 0; i < num_of_filters; i++)
	{
		cout << "Generating filter " << i+1 << endl; 
		int red = rand() % 256;
		int blue = rand() % 256;
		int green = rand() % 256;
		int alpha =  rand() % 50;
		CImg<unsigned char> filter(640, 480, 1, 4);
		cimg_forXY(filter, x, y)
		{
			filter(x, y, 0, 0) = red;
			filter(x, y, 0, 1) = green;
			filter(x, y, 0, 2) = blue;
			filter(x, y, 0, 3) = alpha;
		}
		filter_set.push_back(filter);
	}

	return filter_set;
}

CImg<unsigned char>Filter::merge_filter()
{
	CImg<unsigned char> final_filter(640,480,1,4);

	int index1 = rand() % filter_set.size();
	int index2 = rand() % filter_set.size();
	int index3 = rand() % filter_set.size();

	cimg_forXY(final_filter, x, y)
	{
		final_filter(x, y, 0, 0) = (filter_set.at(index1)(x, y, 0, 0) + filter_set.at(index2)(x, y, 0, 0) + filter_set.at(index3)(x, y, 0, 0))/3;
		final_filter(x, y, 0, 1) = (filter_set.at(index1)(x, y, 0, 1) + filter_set.at(index2)(x, y, 0, 1) + filter_set.at(index3)(x, y, 0, 1))/3;
		final_filter(x, y, 0, 2) = (filter_set.at(index1)(x, y, 0, 2) + filter_set.at(index2)(x, y, 0, 2) + filter_set.at(index3)(x, y, 0, 2))/3;
		final_filter(x, y, 0, 3) = (filter_set.at(index1)(x, y, 0, 3) + filter_set.at(index2)(x, y, 0, 3) + filter_set.at(index3)(x, y, 0, 3))/3;
 
	}
	return final_filter;
}

