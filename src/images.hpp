////////////////////////////////////////
// Program: Gezi Training Data Generator
// File: images.hpp
////////////////////////////////////////



#ifndef IMAGES_HPP
#define IMAGES_HPP

#include <iostream>
#include <string>
#include <vector>
#include <fstream>

#define cimg_use_png 1
#define cimg_display 1
#include "../bin/CImg.h"

using namespace std;
using namespace cimg_library;

class Images 
{
	private:
		
	public:	
	
	CImg<unsigned char> final_product; //layer background and filter on top
	double x;
	double y;
	double angle; //angle CCW from positive x-axis in radians
	double box_x;
	double box_y;	
	double box_width;
	double box_height;	

	CImg<unsigned char> merge_images(string bg_path, string marker_path, CImg<unsigned char> filter);
	CImg<unsigned char> rotate(CImg<unsigned char> image_before, double angle);
};

void make_annotations(vector<vector<double> > annotations_vec, string annotations_folder);

#endif
