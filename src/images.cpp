////////////////////////////////////////
// Program: Gezi Training Data Generator
// File: images.cpp
////////////////////////////////////////

#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>
#include <fstream>
#include <math.h>

#define cimg_use_png 1
#define cimg_display 1
#include "../bin/CImg.h"

#include "images.hpp"

using namespace std;
using namespace cimg_library;

CImg<unsigned char> Images::merge_images(string bg_path, string marker_path, CImg<unsigned char> filter)
{
	CImg<unsigned char> base_image(480, 480, 1, 3);
	
	CImg<unsigned char> bg_image(bg_path.c_str());
	CImg<unsigned char> marker_image(marker_path.c_str());
	
	//background left-right shift
 	int shift = -(rand() % 30);
	
	//marker position
	x = rand() % 480;
	y = rand() % 480;
	angle = (double)rand()/(double)RAND_MAX * 2*M_PI;
	
	int marker_width = marker_image.width();
	int marker_height = marker_image.height();

	//marker rotation
	marker_image = rotate(marker_image, angle);	
	
	//splits marker into components
	CImg<unsigned char> marker_rgb = marker_image.get_shared_channels(0, 2);
	CImg<unsigned char> marker_a = marker_image.get_shared_channel(3);	
	//splits filter into components
	CImg<unsigned char> filter_rgb = filter.get_shared_channels(0, 2);	
	CImg<unsigned char> filter_a = filter.get_shared_channel(3);


	//draws markers and applies filters
	bg_image.draw_image((x - shift) - marker_width, y - marker_height, marker_rgb, marker_a, 1, 255);//minus marker_with instead of marker_width/2
	bg_image.draw_image(shift, 0, filter_rgb, filter_a, 1, 255);
	
	//draws background on base
	base_image.draw_image(shift, 0, 0, 0, bg_image, 1);

	//computes cell that contains the marker center. cells are 
	box_x = x/480;
	box_y = y/480;
	box_width = double(marker_width)/480;
	box_height = double(marker_height)/480;
	
	cimg_forXY(base_image, x, y)
	{
		if (x == box_x * 480 || y == box_y*480)
		{
			base_image(x, y, 0, 0) = 255;
		}
	};
		
	return base_image;	
}

CImg<unsigned char> Images::rotate(CImg<unsigned char> image_before, double angle)
{
	int width = image_before.width();
	int height = image_before.height();
	float center_axis_x = floor(width/2);
	float center_axis_y = floor(height/2);
	int temp_var;
	vector<double> ini_pixel_vec = {0, 0, 0};
	vector<double> transformed_pixel_vec = {0, 0, 0};
	CImg <unsigned char> image_after(width*2, height*2, 1, 4);
	
	cimg_forXY(image_after, x, y)
	{
		//image_after(x, y, 0, 3) = 0;
	};
	
	vector<double> rotation_matrix = 
	{
		cos(angle), -sin(angle),
		sin(angle),  cos(angle)
	};
	int rotation_matrix_size = 3;

	for (double i = 0; i < height; i++)
	{
		for (double j = 0; j < width; j++)
		{
			for (double k = 0; k < 4; k++)
			{
				//cout << i << ", " << j << ", " << k << endl;
				temp_var = image_before(j, i, 0, k); //gets the pixel value of that cell and stores it temporarily
				ini_pixel_vec = {j - center_axis_x, i-center_axis_y, k};
				transformed_pixel_vec[0] = rotation_matrix[0]*ini_pixel_vec[0] + rotation_matrix[1]*ini_pixel_vec[1];
				transformed_pixel_vec[1] = rotation_matrix[2]*ini_pixel_vec[0] + rotation_matrix[3]*ini_pixel_vec[1];
				image_after(transformed_pixel_vec[0] + floor(image_after.width()/2), transformed_pixel_vec[1] + floor(image_after.height()/2), 0, k) = temp_var;				

			}
		}
	}
	return image_after;

}



void make_annotations(vector<vector<double> > annotations_vec,string annotations_folder)
{	
	cout << "Generating annotations.py" << endl;
	
	string annotations_filename = annotations_folder + "annotations.py";
	ofstream annotations;
	annotations.open(annotations_filename);
	annotations << "annotations_list = [ \n";
	
	for (int i = 0; i < annotations_vec.size(); i++)
	{
		annotations << "	[";
		for (int j = 0; j < annotations_vec[i].size(); j++)
		{
			annotations << annotations_vec[i][j];
			if (j != annotations_vec[i].size() - 1)
			{
				annotations << ",";
			}
		}
		annotations << "]";
		if (i != annotations_vec.size() - 1)
		{
			annotations << ",\n";
		}
		else
		{
			annotations << "\n";
		}
	}
	annotations << "]";
	annotations.close();
}
