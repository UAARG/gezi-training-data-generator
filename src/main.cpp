//////////////////////////////////////////////
// Program: Gezi Training Data Generator
// File: main.cpp
//////////////////////////////////////////////


#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>
#include <fstream>


#define cimg_use_png 1
#define cimg_display 1
#include "../bin/CImg.h"
#include "png.h"




#include "filters.hpp"
#include "images.hpp"



using namespace std;
using namespace cimg_library;



using namespace std;

int main()
{	

	int num_of_bg = 10;
	int num_of_marker = 9;
	int num_of_images = 50;


	//creates filter set. Filters are not directly used, but mixed.
	Filter filter_mgr;
	filter_mgr.filter_set = filter_mgr.create_filters(10);	 //makes 500 filters
	
	//vector that stores the positions of the markers
	vector<vector<double> > annotations;
	
	//loops for the number of images
	for (int i = 0; i < num_of_images; i++)
	{	
		cout << "Saving image " << i << ".png" << endl;
		//mix filters
		CImg<unsigned char> filter = filter_mgr.merge_filter();
		//new image object
		Images image;
		//background path
		int bg_index = rand() % num_of_bg;
		string bg_path = "Images/Backgrounds/" + to_string(bg_index) + ".png";
		//marker path
		int marker_index = rand() % num_of_marker;
		string marker_path = "Images/Markers/" + to_string(marker_index) + ".png";
		//combine BG, filter and Marker --> image.final_product
		image.final_product = image.merge_images(bg_path, marker_path, filter);
		//save
		if (double(i)/double(num_of_images) > 0.95)
		{
			string save_path = "Images/Processed Images/testing/" + to_string(i) + ".png";
			image.final_product.save_png(save_path.c_str());		
		} 
		else if (double(i)/double(num_of_images) > 0.85 && i/num_of_images <= 0.95)
		{
			string save_path = "Images/Processed Images/validating/" + to_string(i) + ".png";
			image.final_product.save_png(save_path.c_str());
		}
		else if (double(i)/double(num_of_images) <= 0.85)
		{	
			string save_path = "Images/Processed Images/training/" + to_string(i) + ".png";
			image.final_product.save_png(save_path.c_str()); 
		}
		annotations.push_back({image.box_x, image.box_y, image.box_width, image.box_height, image.angle * 180 / M_PI});
	}

	string annotations_folder = "Images/annotations/";
	make_annotations(annotations, annotations_folder);
	cout << "Done!" << endl;			
 	return 0;
}
